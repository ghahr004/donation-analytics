# First we import numpy as we are going to use it in this project.
import numpy as np
import math
import sys
itcont = sys.argv[1]
percentile = int(open(str(sys.argv[2])).read())
rp = str(sys.argv[3])

with open(itcont) as f:
	data = f.readlines()

# print(data[0]) gives C00629618|N|TER|P|201701230300133512|15C|IND|PEREZ, JOHN A|LOS ANGELES|CA|90017|PRINCIPAL|DOUBLE NICKEL ADVISORS|01032017|40
# |H6CA34245|SA01251735122|1141239|||2012520171368850783
# Based on the instructions given for this project, we are interested in CMTE_ID, NAME, ZIP_CODE, TRANSACTION_DT,
# TRANSACTION_AMT, OTHER_ID. These materials are in 1st, 8th, 11th, 14th, 15th and 16th part of each data line. 
# The input files do have some considerations. We write the following fuctions to satify the Input file consideraions.

# In order to keep the order of our data, we enumerate them. 
 
data_enumerated = list(enumerate(data,1))

# The command print(data_enumerated[0][1]) gives: (1, 'C00629618|N|TER|P|201701230300133512|15C|IND|PEREZ, JOHN A|LOS ANGELES|CA|90017
# |PRINCIPAL|DOUBLE NICKEL ADVISORS|01032017|40|H6CA34245|SA01251735122|1141239|||2012520171368850783\n'). Therefore, data_enumerated is a list of
# elements of the form (number,line) where the lines are in the data set.

# emi stands for empty malformed indetifier. In emiTRANSACTION_DT we identify if TRANSACTION_DT which stands for
# transaction date is non-empty and is of the the valid from MMDDYYYY. We take the pleasant case where the 
# transaction date is valid as 0 and the invalid case to be 1. 
def emiTRANSACTION_DT(dictionary):
	if (len(dictionary['TRANSACTION_DT']) == 8 and
     int(dictionary['TRANSACTION_DT'][0:2]) in range(1,13) and
      int(dictionary['TRANSACTION_DT'][2:4]) in range(1,32) and
       int(dictionary['TRANSACTION_DT'][4:9]) in range(2019)):
		return True
	else:
		return False

# We define the empty_identifier function which will identify empty lists, string or dictionaries.

def empty_identifier(x):
	if len(x) == 0:
		return False
	else:
		return True

# We need to pick the ZIP_CODE s that more than file characters. The fllowing function
# will give 1 for the ZIP_CODE s that are less than 5 characters and 0 for the ones that are 5 characters or more.

def emi_ZIP_CODE(dictionary):
	if len(dictionary['ZIP_CODE']) > 4:
		return True
	else:
		return False


def emi_OTHER_ID(dictionary):
    if len(dictionary['OTHER_ID']) == 0:
        return True
    else:
        return False



# The dictionaries that get True from the following function, satisfy the Input file considerations.

def emi(dictionary):
    return(emiTRANSACTION_DT(dictionary) and
           empty_identifier(dictionary['TRANSACTION_AMT']) and
           empty_identifier(dictionary['CMTE_ID']) and
           empty_identifier(dictionary['NAME']) and
           emi_OTHER_ID(dictionary))
		  

			

# Using the functions we defined, we find the relevant elements of our data the satify the input considerations.      

def relevant_field_builder(data_enumerated):
    list_of_relevant_data = []
    for line in data_enumerated:
        line_splited = line[1].split('|')
        relevant_dic = {'CMTE_ID':line_splited[0],
                        'NAME':line_splited[7],
                        'ZIP_CODE':line_splited[10][0:5],
                        'TRANSACTION_DT':line_splited[13],
                        'TRANSACTION_AMT':line_splited[14],
                        'OTHER_ID':line_splited[15]}
        if emi(relevant_dic) == True:
            list_of_relevant_data.append((line[0],relevant_dic))
    return list_of_relevant_data    
		

# print(relevant_field_builder(data_enumerated)[0]) gives: 
# (2, {'CMTE_ID': 'C00177436', 'NAME': 'DEEHAN, WILLIAM N', 'TRANSACTION_AMT': '384', 'TRANSACTION_DT': '01312017', 'OTHER_ID': '', 'ZIP_CODE': '30004'})
# Where the first element is the number of data in enumerated_data while the second component is a dictionary consisted of materials that
# we need to complete the project. Materials such is CMTE_ID, NAME, ZIP_CODE, TRANSACTION_DT, TRANSACTION_AMT and OTHER_ID. Clearly the first
# two elements in the data_enumerated are ignored by this function as they do not satify Input considerations. 

# Having filtered our data, we need to find the repeat donors. A repeat donor is identified by their NAME and ZIP_CODE and if they have contributed in 
# multiple years. In order to identify them, we use elements of relevant_field_builder(data) where NAME and ZIP_CODE sit for the keys while
# the whole member of the list sits for value. Quite similar to the famous word count problem
# the following function appends the dictionaries of relevant_field_builder(data_enumertad) if they have the same 
# NAME and ZIP_CODE and puts them into the values while it sets (NAME and ZIP_CODE) as its keys.

def dict_of_keys_NAMEandZIP_CODE(data_enumerated):
    T = {}
    for a in relevant_field_builder(data_enumerated):
        if (a[1]['NAME'],a[1]['ZIP_CODE']) not in T.keys():
            T[(a[1]['NAME'],a[1]['ZIP_CODE'])] = [a]
        else:
            T[(a[1]['NAME'],a[1]['ZIP_CODE'])].append(a)
    return T       

# Let's have look at what the output of the function dictionary_with_keys_NAMEandZIP_CODE(data_enumerated) looks like. 
# print(dict_of_keys_NAMEandZIP_CODE(data_enumerated).items()[0]) gives:
#(('SABOURIN, JAMES', '02895'), [(4, {'CMTE_ID': 'C00384516', 'NAME': 'SABOURIN, JAMES', 'TRANSACTION_AMT': '230', 'TRANSACTION_DT': '01312017', 'OTHER_ID': '', 'ZIP_CODE': '02895'}),
#(7, {'CMTE_ID': 'C00384516', 'NAME': 'SABOURIN, JAMES', 'TRANSACTION_AMT': '384', 'TRANSACTION_DT': '01312018', 'OTHER_ID': '', 'ZIP_CODE': '02895'})])
# Therefore dict_of_keys_NAMEandZIP_CODE(data_enumerated) is a dictionary whose keys are (NAME,ZIP_CODE) while the 
# values are data_enumerated elements. Based on the defnition of repeat donor, the elements of dictionary_with_keys_NAMEandZIP_CODE(data_enumerated)
# whose values are donated in more than one year qualify for being a repeat donor. We need the following function to identify the years of 
# donations.

def year_collector(list_of_dicts):
    S = []
    for a in list_of_dicts:
        if int(a[1]['TRANSACTION_DT'][-4:]) not in S:
            S.append(int(a[1]['TRANSACTION_DT'][-4:]))
    return S 

# Lets test the function year_collector.

# print(year_collector([(4,{'CMTE_ID': 'C00384516','NAME': 'SABOURIN, JAMES','OTHER_ID': '','TRANSACTION_AMT': '230','TRANSACTION_DT': '01312017','ZIP_CODE': '02895'}),
# (7,{'CMTE_ID': 'C00384516','NAME': 'SABOURIN, JAMES','OTHER_ID': '','TRANSACTION_AMT': '384','TRANSACTION_DT': '01312018','ZIP_CODE': '02895'})])) 
# gives [2017, 2018] 
# A repeat donor is a donor who has contributed in at least two different calender years. The function 
# dict_of_keys_NAMEandZIP_CODE(data_enumerated) sets (NAME,ZIP_CODE) for the keys and all of the donation
# inforamtion as the value. Therefore the ones whose value has different years of donations qualify
# to be a repeat donor. Hence we have the following function.

repeat_donor = {k:v for k,v in dict_of_keys_NAMEandZIP_CODE(data_enumerated).items() if len(year_collector(v))>1} 

# Based on the definition of nearest rank method given on Wikipedia page https://en.wikipedia.org/wiki/Percentile#The_nearest-rank_method
# we define the percentile function.

def mypercentile(data, P):
    data_sorted = sorted(data)
    N = len(data_sorted)
    n = int(math.ceil((P/100.0)*N))-1
    return data_sorted[n]

def almost_output(dictionary):
    info = []
    for v in dictionary.values():
        t = max(year_collector(v))
        # we find the latest contribution made by the donator.
        list_of_donations = []
        for b in v:
            # as the values in the dictionary repeat_donor is of the form (NAME, ZIP_CODE), we find t which is the latest year the contributer has
            # donated then based on CMTE_ID of all of repeat donors we find the list of contribuions given to the recipient with ID, CMTE_ID.
            if int(b[1]['TRANSACTION_DT'][-4:]) == t:
                CMTE_ID = b[1]['CMTE_ID']
                ZIP_CODE = b[1]['ZIP_CODE']
                index = int(b[0])
                for v in dictionary.values():
                    for c in v:
                        if (c[1]['CMTE_ID'] == CMTE_ID and
                         c[1]['ZIP_CODE'] == ZIP_CODE and
                          int(c[1]['TRANSACTION_DT'][-4:]) == t and
                           int(c[0]) <= index):
                            list_of_donations.append(int(c[1]['TRANSACTION_AMT']))
        info.append([CMTE_ID, ZIP_CODE,str(t),
            str(mypercentile(list_of_donations,
                percentile)),str(sum(list_of_donations)),
            str(len(list_of_donations)),
            list_of_donations,index])
    return info      



almost_output(repeat_donor)

output = open(rp,'w')

result = []
for a in almost_output(repeat_donor):
    result.append('|'.join(a[:6]))

for a in result[::-1]:
    output.write(a + "\n")

output.close()