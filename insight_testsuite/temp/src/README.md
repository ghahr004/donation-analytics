Peyman G, [Feb 12, 2018, 9:21:34 PM]:
## Donation Analytics

In this project we are interested in analyzing the loyalty trends in individual campaign contributions. The input is consisted of two materials. 
* A data set of individual contributions stored in itcont.txt
* The percentile which is stored in percentile.txt is a number from 0 to 100. We are interested in finding the percentiles that recipients have got
from repeat donor individuals.

The data is taken from the Federal Election Commission. https://classic.fec.gov/finance/disclosure/ftpdet.shtml## Code Example which is updated regularly. In this project, if a donor had previously contributed to any recipient in the list itcont.txt file in any prior calendar year, that donor is considered to be a repeat donor. 

A sample of the input data is of the form
> **C00629618**|N|TER|P|201701230300133512|15C|IND|**PEREZ, JOHN A**|LOS ANGELES|CA|**90017**|PRINCIPAL|DOUBLE NICKEL ADVISORS|**01032017**|**40**|**H6CA34245**|SA01251735122|1141239|||2012520171368850783

There are 21 columns in each line of the data set which are seperated from each other by |. We are only interested in 

* recipient of the contribution (or CMTE_ID from the input file)
* 5-digit zip code of the contributor (or the first five characters of the ZIP_CODE field from the input file)
* 4-digit year of the contribution
* running percentile of contributions received from repeat donors to a recipient streamed in so far for this zip code and calendar year. Percentile calculations should be rounded to the whole dollar (drop anything below $.50 and round anything from $.50 and up to the next dollar) 
* total amount of contributions received by recipient from the contributor's zip code streamed in so far in this calendar year from repeat donors
* total number of transactions received by recipient from the contributor's zip code streamed in so far this calendar year from repeat donors

Therefore in the above line of data we just focus on 
> **C00629618**|N|TER|P|201701230300133512|15C|IND|**PEREZ, JOHN A**|LOS ANGELES|CA|**90017**|PRINCIPAL|DOUBLE NICKEL ADVISORS|**01032017**|**40**|**H6CA34245**|SA01251735122|1141239|||2012520171368850783
Where:

C00629618 stands for CMTE_ID, Filer Identification Number 

PEREZ, JOHN A stands for the name of the donator 

90017 denotes the zip code of the donator

01032017  represents the date of the transaction

40 indicates the amount of contribution 

H6CA34245 stands for other identification number




## Summery of Approach 

Since the order of the input file is important for us, we first enumerate our data set by the function list(enumerate(data,1)) where data is the lines in the input file itcont.txt. Since we are only interested in finding CMTE_ID, NAME, ZIP_CODE, TRANSACTION_DT,
TRANSACTION_AMT, OTHER_ID in our dataset, we define some functions which check if these strings do satisfy the input file considerations. We define:

* emiTRANSACTION_DT which checks if a transaction date is valid and non-empty( if it is of the form MMDDYYYY)
*  empty_identifier which checks if an instance is empty or not. It gives False for empty instances and True for non-empty ones
* emi_ZIP_CODE which checks if the ZIP_CODE has 5 numbers or above.
* emi_OTHER_ID which checks if OTHER_ID of the donors is empty or not. It gives True if the OTHER_ID is empty and gives False if OTHER_ID is not empty. 
* emi checks if all of the functions above are giving True or not. If the answer is True it means that the data line satisfies the Input file considerations.

We first extract the materials we are interested from the data lines which are CMTE_ID, NAME, ZIP_CODE, TRANSACTION_DT,
TRANSACTION_AMT, OTHER_ID. These materials are in 1st, 8th, 11th, 14th, 15th and 16th part of each data line. Since these materials are in data lines and are separated from each other by  |, we use the built-in function split(|) to split the instances in each data line. The function relevant_field_builder  gives a list in which the elements are of the form  (number of the line, dictionary which keys to be CMTE_ID, NAME, ZIP_CODE, TRANSACTION_DT, TRANSACTION_AMT,OTHER_ID and the values are the corresponding strings in the corresponding data line). 

Having filtered our data, we need to find the repeat donors. A repeat donor is identified by their NAME and ZIP_CODE and if they have contributed in 
 multiple years. In order to identify them, we use elements of relevant_field_builder(data) where NAME and ZIP_CODE sit for the keys while
 the whole member of the list sits for value. Quite similar to the famous word count problem
 the function dict_of_keys_NAMEandZIP_CODE(data_enumerated)  appends the elements of relevant_field_builder(data_enumertad) if they have the same 
 NAME and ZIP_CODE and puts them into the values while it sets (NAME and ZIP_CODE) as its keys. 

dict_of_keys_NAMEandZIP_CODE(data_enumerated).items()[0] gives:


(('SABOURIN, JAMES', '02895'), [(4, {'CMTE_ID': 'C00384516', 'NAME': 'SABOURIN, JAMES', 'TRANSACTION_AMT': '230', 'TRANSACTION_DT': '01312017', 'OTHER_ID': '', 'ZIP_CODE': '02895'}),
(7, {'CMTE_ID': 'C00384516', 'NAME': 'SABOURIN, JAMES', 'TRANSACTION_AMT': '384', 'TRANSACTION_DT': '01312018', 'OTHER_ID': '', 'ZIP_CODE': '02895'})])  
Therefore dict_of_keys_NAMEandZIP_CODE(data_enumerated).items()[0]  is a dictionary whose keys are (NAME,ZIP_CODE)  while the 
 values are data_enumerated elements. Based on the definition of repeat donor, the elements of dict_with_keys_NAMEandZIP_CODE(data_enumerated)
 whose values are donated in more than one year qualify for being a repeat donor. We need a function the collects the years of donations for the donors that have donated more than once. The function year_collector(list_of_dicts) collects the years of 
donation that a single donor has made. Clearly, based on the definition of repeat donor, the elements of dict_of_keys_NAMEandZIP_CODE(data_enumerated).items() which have more than one element when they are given to the function year_collector(list_of_dicts). 

The function almost_output(dictionary) gives all of the information we need from our data. It does its calculations on the set of repeat donors. As the set of repeat donors is a dictionary whose keys are (NAME, ZIP_CODE) they are not any important for us, therefore we reduce our attention to the values. For each value given in repeat_donor, we find the last year that the donor has contributed then based on the CMTE_ID of the recipient that the donor has contributed, by setting up a loop on the repeat_donor values we identify the ones that have the same 'CMTE_ID` and their index in the original data set is less than the index of the donor.